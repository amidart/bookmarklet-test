(function(){
  // the minimum version of jQuery we want
  var v = "2.1.1";
  if (window.jQuery === undefined || window.jQuery.fn.jquery < v) {
    var done = false;
    var script = document.createElement("script");
    script.src = "http://ajax.googleapis.com/ajax/libs/jquery/" + v + "/jquery.min.js";
    script.onload = script.onreadystatechange = function(){
      if (!done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {
        done = true;
        initMyBookmarklet();
      }
    };
    document.getElementsByTagName("head")[0].appendChild(script);
  } else {
    initMyBookmarklet();
  }

  /**
   * Main Function
   */
  function initMyBookmarklet() {
    window.myBookmarklet = (function() {
      getData();
    })();
  }


  function getData() {
    var url = document.location.href;
    var title = document.title;
    var text = getSelectionText();
    var res = 'Url: ' + url + '\n\n' + 'Title: ' + title + '\n\n';
    if (text) res += 'Text: ' + text;
    alert(res);
  }


  /**
   * Get selected text on the page
   * @return {string} text
   */
  function getSelectionText() {
      var text = "";
      if (window.getSelection) {
          text = window.getSelection().toString();
      } else if (document.selection && document.selection.type != "Control") {
          text = document.selection.createRange().text;
      }
      return text;
  }



})();


